from rest_framework import serializers
from .models import Stock


class StockSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Stock
        fields = '__all__'
        # fields = ('id','ticker', 'open', 'close', 'volume')
